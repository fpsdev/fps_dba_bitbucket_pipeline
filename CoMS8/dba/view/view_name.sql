
/*

    Create new VIEW for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 

Steps 

    1. Update META data about SQL object 
    2. CREATE OR REPLACE VIEW
    3. Permissions 

*/

-----------------------------------------------------------------------------------------------------------------------------------------------

/*
     1. UPDATE META DATA about the object 
*/

INSERT INTO dba.db_objects_master_data
VALUES
  (
              'freetier'                                   -- instance_name
            , 'dba'                                        -- schema_name
            , 'vw_user_tables'                               -- object_name
            , 'in-use'                                     -- status
            , 'General'                                    -- client_name 
            , 'Sathish.Kothandam;'                         -- technical_owner
            , 'Vyomesh.Tala;'                              -- business_owner
            , 'General'                                    -- business need 
            , 'view'                                        -- type of object 
            , 'view'                                        -- sub type of object 
            , NOW()                                        -- table created datetime 
            , NOW()                                        -- table last updated datetime  
            , NULL                                         -- inception date 
            , NULL                                         -- decommission date 
            , NULL                                         -- age as per last updated date  
            , 'This view used to calculate table no of rows and size' -- comments 
                )
ON DUPLICATE KEY UPDATE
            client_name = values(client_name),
            technical_owner = values(technical_owner),
            business_owner = values(business_owner),
            business_need = values(business_need),
            type_of_object = values(type_of_object),
            sub_type_of_object = values(sub_type_of_object),
            table_create_time = values(table_create_time),
            table_last_updated = values(table_last_updated),
            inception_date = values(inception_date),
            decommission_date = values(decommission_date),
            last_updated_age_in_years = values(last_updated_age_in_years),
            comments = values(comments);

/*
---------------------------------------------------------------------------------------
    2. CREATE OR REPLACE VIEW 
 */
 
DROP VIEW IF EXISTS dba.vw_user_tables ; 

CREATE VIEW dba.vw_user_tables
AS
    SELECT
        `tables`.`TABLE_SCHEMA`                                                       AS `table_schema` ,
        `tables`.`TABLE_NAME`                                                         AS `table_name`   ,
        `tables`.`TABLE_TYPE`                                                         AS `table_type`   ,
        `tables`.`ENGINE`                                                             AS `engine`       ,
        `tables`.`TABLE_ROWS`                                                         AS `table_rows`   ,
        ROUND((((`tables`.`DATA_LENGTH` + `tables`.`INDEX_LENGTH`) / 1024) / 1024),0) AS `tablemb`
    FROM
        `information_schema`.`TABLES` `tables`
    WHERE 
    /* Exclude system schemas and exclude views */
        (
            (
                `tables`.`TABLE_SCHEMA` NOT IN ('information_schema',
                                                'mysql',
                                                'performance_schema',
                                                'sys'))
        AND
            (
                `tables`.`TABLE_TYPE` <> 'VIEW'))
                ;