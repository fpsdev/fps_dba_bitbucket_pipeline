import re

from dbaPipelineCustomException import CommentValueException
from dbaPipelineCustomException import FolderObjectNameException
from dbaPipelineCustomException import EmailIdValidation

regex = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"


def validateData(
    readFile, folderObjectName, comments, techinalOwnwerEmailId, businessOwnerEmailId
):
    if readFile.find(folderObjectName) < 0:
        raise FolderObjectNameException(folderObjectName)

    if len(comments) < 20:
        raise CommentValueException(comments)

    print(techinalOwnwerEmailId[0])
    for i in range(len(techinalOwnwerEmailId)):
        validateEmailId(techinalOwnwerEmailId[i])

    for i in range(len(businessOwnerEmailId)):
        validateEmailId(businessOwnerEmailId[i])


def validateEmailId(email):
    if not re.search(regex, email):
        raise EmailIdValidation(email)
