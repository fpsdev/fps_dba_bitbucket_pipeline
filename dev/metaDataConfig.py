import re   

class metaDataConfig:
    def __init__(self, user_edit):
        self._client_name =  user_edit.split("=")[1].replace("technical_owner", "").strip()
        self._technical_owner = user_edit.split("=")[2].replace("business_owner", "").strip()
        self._business_owner = user_edit.split("=")[3].replace("business_need", "").strip()
        self._business_need = user_edit.split("=")[4].replace("sub_type_of_object", "").strip()
        self._sub_type_of_object = user_edit.split("=")[5].replace("decommission_date", "").strip()
        # self._decommission_date = user_edit.split("=")[6].replace("comments", "").strip()
        self._comments = user_edit.split("=")[7].strip()        
        self._status = "in-use"

    def get_client_name(self):
        return self._client_name

    def get_technical_owner(self):
        self._technical_owner = re.findall(r"([^(,)]+)(?!.*\()", str(self._technical_owner))
        for i in range(len(self._technical_owner)):
            self._technical_owner[i] = self._technical_owner[i].strip("'").strip()
        return self._technical_owner

    def get_business_owner(self):
        self._business_owner = re.findall(r"([^(,)]+)(?!.*\()", str(self._business_owner))
        for i in range(len(self._business_owner)):
            self._business_owner[i] = self._business_owner[i].strip("'").strip()
        return self._business_owner

    def get_business_need(self):
        return self._business_need

    def get_sub_type_of_need(self):
        return self._sub_type_of_object

    def get_decommission_date(self):
        return self._decommission_date

    def get_comments(self):
        return self._comments

    def get_status(self):
        return self._status
